[Source](https://droiduino-cc.medium.com/changing-arduino-bluetooth-name-if-you-are-using-hc05-module-6f184a242467)

# Uploading Arduino Code

Before start connecting HC05 to the board, we should upload the code that will give access to AT command mode first. Just Paste the code below to Arduino IDE.

## Code Sample

``` ino
#include <SoftwareSerial.h>

  

SoftwareSerial BTSerial(10, 11); // RX | TX

  

void setup()

{

pinMode(9, OUTPUT);

digitalWrite(9, HIGH);

Serial.begin(9600);

Serial.println("Enter AT commands:");

BTSerial.begin(38400); // HC-05 default speed in AT command mode

}

  

void loop()

{

// Read from HC05 and send to Arduino

if (BTSerial.available())

Serial.write(BTSerial.read());

  

// Read from serial monitor and send to HC05

if (Serial.available())

BTSerial.write(Serial.read());

}

```

## Key Pin

![1](images/bt_key_pin.png)

## Schemantics
![2](images/bluetooth_defaults.png)

# Accessing AT Command Mode

Follow these steps to get to the AT Command mode :

1.  Disconnect +5V from HC05
2.  Connect Arduino to your computer
3.  Reconnect +5V to HC05
4.  The LED on HC05 should be blinking slowly now. If it is not blinking, it is blinking fast or if it is turned off, you should check the wiring and connection to KEY pin (i.e. make sure the jumper cable touches the metal surface)
5.  Next, open the serial monitor on Android IDE. On the bottom part of the serial monitor, select “Both NL & CR” and set the baud rate to 9600.
6.  To check if everything is connected properly, type “AT” (without the quote) on the input line, and the response should be “OK”, otherwise recheck the wiring.

## Commands
```
AT
AT+NAME=<new device name>
AT+PSWD=<new password)

Result, If it Works
Ok
```
