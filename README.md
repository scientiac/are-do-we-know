# Some Projects on Arduino

## Firefly
A fire detection and extinguisher robot that also sends SMS alerts when it senses gas leakage. 
[check](./firefly/Firefly.ino)

## HC-05 Customization
This took some time to figure out but yes we can finally change the name and password of the HC-O5 Bluetooth module. 
[check](./hc-05/hc-o5.md)

## Bluetooth Car with Relay
Apparently, relays can be used as motor drivers, so we tried making that for the Robo Race, Although the race didn't go as planned we managed to get motors spinning per our commands. 
[check](./relay-motor-driver/relay-motor-driver.md)

## Bluetooth Car with Motor Shield
A little quick start guide to use motor shield to make a Bluetooth car. 
[check](./motor-shield-template/shield.md)
