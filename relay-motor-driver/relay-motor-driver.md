[Reference](https://www.keuwl.com/electronics/rduino/bluet/01-rc-car/)

## Components used

| No. | Item | Quantity |
| --- | --- | --- |
| 1 | Arduino Nano | 1 | 
| 2 | HC-06 Bluetooth Module | 1 |
| 3 | 4 Phase Relay Module | 1 |
| 4 | 10k and 20k Resistors | 1 |
| 5 | Chassis  | 1 | 
| 6 | Jumper Cables | 1 |

![1](images/motor-driver-relay.jpg)




## Sample Code 1
``` ino
char BluetoothData; // the Bluetooth data received 

void setup() {

  Serial.begin(9600);
  
  //Set Digital Pins 4 to 7 as Output
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);
  
  //Set State of Pins all to LOW (0)
  digitalWrite(4,0);
  digitalWrite(5,0);
  digitalWrite(6,0);
  digitalWrite(7,0);

}

void loop() {

  if (Serial.available()){
  
    BluetoothData=Serial.read(); //Get next character from bluetooth
  
    if(BluetoothData=='R'){ // Red Button Pressed
      digitalWrite(4,1); //Turn digital out 4 to High
      digitalWrite(5,0); //Turn digital out 5 to Low
    }
    if(BluetoothData=='Y'){ // Yellow Button Pressed
    digitalWrite(5,1);
    digitalWrite(4,0);
    }
    if(BluetoothData=='r'||BluetoothData=='y'){ // Red or Yellow Button Released
      digitalWrite(4,0);
      digitalWrite(5,0);
    }
    if(BluetoothData=='G'){ // Green Button Pressed
      digitalWrite(6,1);
      digitalWrite(7,0);
    }
    if(BluetoothData=='B'){ // Blue Button Pressed
      digitalWrite(7,1);
      digitalWrite(6,0);
    }
    if(BluetoothData=='g'||BluetoothData=='b'){ // Green or Blue Button Released
      digitalWrite(7,0);
      digitalWrite(6,0);
    }
  
  }
  
  delay(10);// wait 10 ms

}

```
![2](images/relay_motor_controller.png)
## Sample Code 2

``` ino

int RelayPin1 = 8;
int RelayPin2 = 9;
int RelayPin3 = 10;
int RelayPin4 = 11;

void setup()
{
  Serial.begin(4900);
  pinMode(RelayPin1, OUTPUT);
  pinMode(RelayPin2, OUTPUT);
  pinMode(RelayPin3, OUTPUT);
  pinMode(RelayPin4, OUTPUT);

  //TURN OFF all Relays on Start 
  digitalWrite(RelayPin1, HIGH);
  digitalWrite(RelayPin2, HIGH);
  digitalWrite(RelayPin3, HIGH);
  digitalWrite(RelayPin4, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  digitalWrite(RelayPin1, LOW);
  digitalWrite(RelayPin2, HIGH);
  digitalWrite(RelayPin3, HIGH);
  digitalWrite(RelayPin4, LOW);

  delay(5000);
   
  digitalWrite(RelayPin1, LOW);
  digitalWrite(RelayPin2, LOW);
  digitalWrite(RelayPin3, LOW);
  digitalWrite(RelayPin4, LOW);

  delay(5000);
  
}
```
