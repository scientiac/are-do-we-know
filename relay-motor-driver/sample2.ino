int RelayPin1 = 8;
int RelayPin2 = 9;
int RelayPin3 = 10;
int RelayPin4 = 11;

void setup()
{
  Serial.begin(4900);
  pinMode(RelayPin1, OUTPUT);
  pinMode(RelayPin2, OUTPUT);
  pinMode(RelayPin3, OUTPUT);
  pinMode(RelayPin4, OUTPUT);

  //TURN OFF all Relays on Start 
  digitalWrite(RelayPin1, HIGH);
  digitalWrite(RelayPin2, HIGH);
  digitalWrite(RelayPin3, HIGH);
  digitalWrite(RelayPin4, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  digitalWrite(RelayPin1, LOW);
  digitalWrite(RelayPin2, HIGH);
  digitalWrite(RelayPin3, HIGH);
  digitalWrite(RelayPin4, LOW);

  delay(5000);
   
  digitalWrite(RelayPin1, LOW);
  digitalWrite(RelayPin2, LOW);
  digitalWrite(RelayPin3, LOW);
  digitalWrite(RelayPin4, LOW);

  delay(5000);
  
}
